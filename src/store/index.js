import Vue from "vue";
import Vuex from "vuex";
import counter from "./counter";
import alert from "./alert";
import dialog from "./dialog";
import auth from "./auth";
import VuexPersistence from "vuex-persist";

const vuexLocal = new VuexPersistence({
  key: "data-login",
  storage: window.localStorage,
});

Vue.use(Vuex);

export default new Vuex.Store({
  plugins: [vuexLocal.plugin],
  modules: {
    counter,
    alert,
    dialog,
    auth,
  },
});
